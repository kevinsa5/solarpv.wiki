<!-- TITLE: Wiki Meta -->
<!-- SUBTITLE: -->

- [Gitlab Commit History](https://gitlab.com/kevinsa5/solarpv.wiki/commits/master)
- [Online TeX Editor/Viewer](https://arachnoid.com/latex/)
- [MathJax/TeX Cheatsheet](https://math.meta.stackexchange.com/questions/5020/mathjax-basic-tutorial-and-quick-reference?rq=1)
- Remember to include a space after all curly-brace pairs... the wiki parser is buggy


Proposed wiki layout
- System Hardware
	- Inverters
	- Modules
	- Transformers
	- Combiners
	- Racking
	- DAS
		- Met sensors
		- datalogger
		- cell modem
- Performance Evaluation
	- Capacity Testing
	- Metrics
- Energy Modeling
	- TMYs
	- 8760s
	- Software
		- SAM
		- PVsyst
	- IEs
	- Energy Model 
		- Shading
		- IAM
		- etc
- Weather
	- Irradiance
		- POA/GHI/DNI/DHI
	- Temp, wind, etc
- Intro
	- corey's notebook