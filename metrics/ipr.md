<!-- TITLE: IPR -->
<!-- SUBTITLE: The Investor Performance Ratio -->

The Investor Performance Ratio (IPR) measures asset financial performance.  It is defined as the ratio of actual revenue to predicted revenue over a time period (monthly, annually, etc).  