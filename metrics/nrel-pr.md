<!-- TITLE: NREL PR -->
<!-- SUBTITLE: The temperature-corrected NREL Performance Ratio -->

The NREL weather-corrected performance ratio (PR) is a performance metric that compares site production with measured insolation along with an adjustment for measured temperature.  The temperature adjustment significantly reduces seasonality when compared with the standard PR (which has no temperature adjustment).  It is commonly used as a metric of system "health" during longterm system operation, but is also sometimes used as the test metric in capacity tests.  Usually the metric is used in an "Actual vs Expected" context -- calculating the metric using measured weather and production data and comparing against the same calculation done against the system's energy model.  


# Definition

The PR requires a timeseries dataset where each timestamp has values for system output power, POA irradiance, and internal PV cell temperature.  The dataset is filtered prior to any calculations to remove points that may be non-representative of system health.  Timestamps where POA irradiance is less than $100 W/m^2$ are removed and for systems with sufficiently large DC/AC ratios, timestamps when the inverters clip are removed as well.  The filtered dataset is then used in the formula

$$PR = \frac{\sum E_{ac}} {\sum \left[ kW_{dc} \cdot (POA/1000) \cdot (1 - \delta \cdot (T^*_{cell} - T_{cell}))\right]} $$

where

- $$E_{ac} $$ is the measured AC generation of the system for each given time
- $$kW_{dc} $$ is the nameplate DC capacity of the system
- $$POA$$ is the measured POA irradiance for each time
- $$\delta$$ is the temperature coefficient of the installed modules -- measured in %/C
- $$T^*_{cell} $$ is the average cell temperature over one year, weighted by POA
- $$T_{cell} $$ is the estimated cell temperature for each given time

# Discussion

The PR must be calculated using timeseries data in order to filter the dataset appropriately.  Thus, longterm aggregates are insufficient to calculate the formula.  

## Cell Temperature

Since cell temperature is difficult to measure, most implementations of the NREL PR use heat flow models to estimate internal cell temperature from more commonly measured weather parameters.  One commonly used model developed by Sandia is the two-step formula

$$T_{mod} = T_{amb} + POA \cdot e^{a + b \cdot v} $$
$$T_{cell} = T_{mod} + (POA/1000) \cdot \Delta T $$

where

- $$T_{amb} $$ is the measured ambient temperature in Celsius
- $$T_{mod} $$ is the estimated module surface temperature in Celsius
- $$T_{cell} $$ is the estimated internal PV cell temperature
- $$POA$$ is the measured POA irradiance in $$W/m^2$$
- $$v$$ is the measured wind speed in $$m/s$$
- $$a, b, \Delta T$$ are module-dependent heat flow parameters 

PVsyst uses a different cell temperature formula.

## Subinterval Clipping

The timescale of the dataset can have significant effect on the metric.  In particular, using low-resolution data (eg hourly averages) can cause nonlinearities in system behavior to cause apparent underperformance when using this metric.  For example, an hour with highly variable irradiance conditions (perhaps caused by brief, intermittent cloud cover) can have short-timescale periods of inverter clipping.  When taking the average irradiance and average output power over the hour, the low-irradiance times will cause the averages to pass through the filtering (ie, the hour will not look like a clipping hour), and the energy generation will lag behind the measured insolation, making it look like the site is underperforming when it is not.  


# Tutorial
The nameplate capacity of a module is what the module will produce at $1000 W/m^2$ and 20C.  Since the output power of a pv module is close to linear with POA irradiance, we have

$$\text{module output power} = (\text{nameplate capacity}) * (POA / 1000)$$

For instance, at $500 W/m^2$, a 300W panel will produce (300W) * (500 / 1000) = 150W -- half of its nameplate:

![Figure 1](/uploads/figure-1.png "Output power linear with POA")

This is a crude estimate of how much power you expect a solar farm to produce at a given irradiance level.  Since there's not much of a difference between one panel and an entire array (other than the DC nameplate), you can apply the same formula to an entire array -- a 5 MW DC nameplate system will produce 2.5 MW at $500 W/m^2$.  

However, this model only tells part of the story.  Real-life solar panels get less efficient as they get hotter -- on the order of $0.5\%/C$.  What this means is that if a given solar panel is producing 100 W at a certain irradiance level, it will produce 100.5 W if the panel gets 1C cooler or 99.5 W if it gets 1C hotter.  Typically, pv panels operate pretty hot, and since the baseline is at 20C, it's important to take the temperature effect into account:

![Figure 2](/uploads/figure-2.png "Figure 2")

To calculate the exact impact of temperature on module efficiency, you need to know the module's heat coefficient $\delta$.  This value depends on the type of module -- thin film is less influenced by temperature than silicon.  Since $\delta$ is a % loss per degree Celsius, you calculate how many degrees Celsius to account for (eg if the pv cells are operating at 60C, then the difference is 60-20 = 40C) and multiply.  For instance, typical silicon polycrystalline cells have $\delta=-0.43\%/C$, which gives a $0.43 \cdot 40 = 17.2\%$ efficiency loss!  

If you combine everything so far into one equation, you get

$$\text{output power} = kW_{dc} \cdot (POA/1000) \cdot (1 - \delta \cdot (T^*_{cell} - T_{cell}))$$, 

which is what we'll call the system's "expected output power" for a given weather measurement.  This can then be compared with what the system *actually* produced during the same time period to monitor system performance -- typically, by dividing the two to form a ratio that is basically out of 100%.  Since irradiance and temperature are the two main "uncontrollable" sources of loss, by including adjustments for them in the metric, it is restricted to only flag issues that can be solved (for instance, offline combiner boxes, shading, snow coverage, etc).   

However, the noise in calculating this ratio for any given moment can be high -- for instance, if a cloud is partly over the array but not the irradiance sensor, then system generation won't match the "expected output" and the metric will be thrown off.  For this reason, the formula is usually calculated over an extended time period -- days or months -- with the ratio being calculated as the *sum* of actual production divided by the *sum* of expected production across the time period.  

# References
[NREL Paper](https://www.nrel.gov/docs/fy13osti/57991.pdf)
[Sandia PVPMC Module Temperature](https://pvpmc.sandia.gov/modeling-steps/2-dc-module-iv/module-temperature/sandia-module-temperature-model/)
[Sandia PVPMC Cell Temperature](https://pvpmc.sandia.gov/modeling-steps/2-dc-module-iv/cell-temperature/sandia-cell-temperature-model/)
[PVsyst Cell Temperature](http://files.pvsyst.com/help/thermal_loss.htm)
