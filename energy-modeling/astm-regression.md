<!-- TITLE: ASTM Regression -->
<!-- SUBTITLE: The ASTM Regression, used to predict system output from weather data -->

The ASTM regression is a method of predicting system output power from measured weather data.  Specifically, it is a multiple parameter linear regression.  It is a standard used in capacity testing and is defined in ASTM E2848.  

### Definition

The ASTM regression equations uses instantaneous weather data to predict power with 

$$
P = E \cdot (a_1 + a_2 \cdot E + a_3 \cdot T_a + a_4 \cdot v)
$$

where 

- $$P$$ is the system's output power in $$kW$$
- $$E$$ is the measured POA irradiance in $$W/m^2$$
- $$T_a$$ is the measured ambient temperature in $$C$$
- $$v$$ is the measured wind speed in $$m/s$$
- $$a_1, a_2, a_3, a_4$$ are system-specific regression coefficients, calculated by fitting the equation to the system's energy model